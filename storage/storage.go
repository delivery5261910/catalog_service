package storage

import (
	pb "catalog_service/genproto/catalog_service"
	"context"
	"google.golang.org/protobuf/types/known/emptypb"
)

type IStorage interface {
	Close()
	Category() ICategoryStorage
	Product() IProductStorage
}

type ICategoryStorage interface {
	Create(context.Context, *pb.CreateCategoryRequest) (*pb.Category, error)
	Get(context.Context, *pb.PrimaryKey) (*pb.Category, error)
	GetList(context.Context, *pb.GetListRequest) (*pb.CategoryResponse, error)
	Update(context.Context, *pb.Category) (*pb.Category, error)
	Delete(context.Context, *pb.PrimaryKey) (*emptypb.Empty, error)
}

type IProductStorage interface {
	Create(context.Context, *pb.CreateProductRequest) (*pb.Product, error)
	Get(context.Context, *pb.PrimaryKey) (*pb.Product, error)
	GetList(context.Context, *pb.GetListRequest) (*pb.ProductResponse, error)
	Update(context.Context, *pb.Product) (*pb.Product, error)
	Delete(context.Context, *pb.PrimaryKey) (*emptypb.Empty, error)
}
