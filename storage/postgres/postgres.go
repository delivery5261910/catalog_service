package postgres

import (
	"catalog_service/config"
	"catalog_service/pkg/logger"
	"catalog_service/storage"
	"context"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/jackc/pgx/v5/pgxpool"
	"strings"

	_ "github.com/golang-migrate/migrate/v4/database"          //database is needed for migration
	_ "github.com/golang-migrate/migrate/v4/database/postgres" //postgres is used for database
	_ "github.com/golang-migrate/migrate/v4/source/file"       //file is needed for migration url
	_ "github.com/lib/pq"
)

type Store struct {
	db  *pgxpool.Pool
	cfg config.Config
	log logger.ILogger
}

func New(ctx context.Context, cfg config.Config, log logger.ILogger) (storage.IStorage, error) {
	url := fmt.Sprintf(
		`postgres://%s:%s@%s:%s/%s?sslmode=disable`,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
	)

	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil {
		return Store{}, err
	}

	poolConfig.MaxConns = 100

	pool, err := pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil {
		return Store{}, err
	}

	//migration
	m, err := migrate.New("file://migrations/postgres", url)
	if err != nil {
		return Store{}, err
	}

	if err = m.Up(); err != nil {
		if !strings.Contains(err.Error(), "no change") {
			fmt.Println("entered", err)
			version, dirty, err := m.Version()
			if err != nil {
				return Store{}, err
			}

			if dirty {
				version--
				if err = m.Force(int(version)); err != nil {
					return Store{}, err
				}
			}
			return Store{}, err
		}
	}

	return Store{
		db:  pool,
		cfg: cfg,
		log: log,
	}, nil
}

func (s Store) Close() {
	s.db.Close()
}

func (s Store) Category() storage.ICategoryStorage {
	return NewCategoryRepo(s.db, s.log)
}

func (s Store) Product() storage.IProductStorage {
	return NewProductRepo(s.db, s.log)
}
