package postgres

import (
	pb "catalog_service/genproto/catalog_service"
	"catalog_service/pkg/helper"
	"catalog_service/pkg/logger"
	"catalog_service/storage"
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type productRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewProductRepo(db *pgxpool.Pool, log logger.ILogger) storage.IProductStorage {
	return &productRepo{
		db:  db,
		log: log,
	}
}

func (p *productRepo) Create(ctx context.Context, request *pb.CreateProductRequest) (*pb.Product, error) {
	var (
		product         = pb.Product{}
		err             error
		searchingColumn string
	)

	query := `
		insert into products (id, description, status, type, price, category_id, searching_column)
			values ($1, $2, $3, $4, $5, $6, $7)
			returning id, description, order_number,  status, type, price, category_id`

	searchingColumn = fmt.Sprintf("%s %v %s %f %s", request.Description, request.Status, request.Type, request.Price, request.CategoryId)

	if err = p.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.Description,
		request.Status,
		request.Type,
		request.Price,
		request.CategoryId,
		searchingColumn).
		Scan(
			&product.Id,
			&product.Description,
			&product.OrderNumber,
			&product.Status,
			&product.Type,
			&product.Price,
			&product.CategoryId,
		); err != nil {
		fmt.Println("err 3", err.Error())
		return nil, err
	}

	return &product, nil
}

func (p *productRepo) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Product, error) {
	var (
		product = pb.Product{}
	)

	query := `select id, description, order_number,  status, type, price, category_id from products where deleted_at = 0 and status = true and id = $1`

	if err := p.db.QueryRow(ctx, query, key.GetId()).Scan(
		&product.Id,
		&product.Description,
		&product.OrderNumber,
		&product.Status,
		&product.Type,
		&product.Price,
		&product.CategoryId,
	); err != nil {
		p.log.Error("error while scanning product by id", logger.Error(err))
		return nil, err
	}

	return &product, nil
}

func (p *productRepo) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.ProductResponse, error) {
	var (
		resp   = pb.ProductResponse{}
		filter = "where status = true and deleted_at = 0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s' ", request.GetSearch())
	}

	countQuery := `select count(1) from products ` + filter

	if err := p.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		p.log.Error("error while scanning count of products", logger.Error(err))
		return nil, err
	}

	query := `select id, description, order_number, status, type, price, category_id from products ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := p.db.Query(ctx, query)
	if err != nil {
		p.log.Error("error while getting product rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			product = pb.Product{}
		)

		if err = rows.Scan(
			&product.Id,
			&product.Description,
			&product.OrderNumber,
			&product.Status,
			&product.Type,
			&product.Price,
			&product.CategoryId,
		); err != nil {
			p.log.Error("error while scanning ony by one product", logger.Error(err))
			return nil, err
		}

		resp.Products = append(resp.Products, &product)
	}

	resp.Count = count

	return &resp, nil
}

func (p *productRepo) Update(ctx context.Context, product *pb.Product) (*pb.Product, error) {
	var (
		resp   = pb.Product{}
		params = make(map[string]interface{})
		query  = `update products set `
		filter = ""
	)

	params["id"] = product.GetId()

	if product.GetDescription() != "" {

		params["description"] = product.GetDescription()

		filter += " description = @description,"
	}

	if product.GetStatus() {
		params["status"] = product.GetStatus()

		filter += " status = @status,"
	}

	if product.GetType() != "" {
		params["type"] = product.GetType()

		filter += " type = @type,"
	}

	if product.GetPrice() != 0 {
		params["price"] = product.GetPrice()

		filter += " price = @price,"
	}

	if product.GetCategoryId() != "" {
		params["category_id"] = product.GetCategoryId()

		filter += " category_id = @category_id,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, description, order_number, status, type, price, category_id`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := p.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
		&resp.Description,
		&resp.OrderNumber,
		&resp.Status,
		&resp.Type,
		&resp.Price,
		&resp.CategoryId,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (p *productRepo) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	_, err := p.db.Exec(ctx, `update products set deleted_at = extract(epoch from current_timestamp) where id = $1`, key.GetId())

	return nil, err
}
