package service

import (
	pb "catalog_service/genproto/catalog_service"
	"catalog_service/grpc/client"
	"catalog_service/pkg/logger"
	"catalog_service/storage"
	"context"
	"fmt"
	"google.golang.org/protobuf/types/known/emptypb"
)

type categoryService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pb.UnimplementedCategoryServiceServer
}

func NewCategoryService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *categoryService {
	return &categoryService{
		storage:  strg,
		services: services,
		log:      log,
	}
}

func (h *categoryService) Create(ctx context.Context, request *pb.CreateCategoryRequest) (*pb.Category, error) {
	category, err := h.storage.Category().Create(ctx, request)
	if err != nil {
		fmt.Println("Err 1", err.Error())
		return nil, err
	}

	return category, nil
}

func (h *categoryService) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Category, error) {
	fmt.Println(key)
	return h.storage.Category().Get(ctx, key)
}

func (h *categoryService) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.CategoryResponse, error) {
	return h.storage.Category().GetList(ctx, request)
}

func (h *categoryService) Update(ctx context.Context, category *pb.Category) (*pb.Category, error) {
	return h.storage.Category().Update(ctx, category)
}

func (h *categoryService) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	return h.storage.Category().Delete(ctx, key)
}
