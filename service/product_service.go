package service

import (
	pb "catalog_service/genproto/catalog_service"
	"catalog_service/grpc/client"
	"catalog_service/pkg/logger"
	"catalog_service/storage"
	"context"
	"fmt"
	"google.golang.org/protobuf/types/known/emptypb"
)

type productService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pb.UnimplementedProductServiceServer
}

func NewProductService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *productService {
	return &productService{
		storage:  strg,
		services: services,
		log:      log,
	}
}

func (h *productService) Create(ctx context.Context, request *pb.CreateProductRequest) (*pb.Product, error) {
	product, err := h.storage.Product().Create(ctx, request)
	if err != nil {
		fmt.Println("Err 1", err.Error())
		return nil, err
	}

	return product, nil
}

func (h *productService) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Product, error) {
	fmt.Println(key)
	return h.storage.Product().Get(ctx, key)
}

func (h *productService) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.ProductResponse, error) {
	return h.storage.Product().GetList(ctx, request)
}

func (h *productService) Update(ctx context.Context, product *pb.Product) (*pb.Product, error) {
	return h.storage.Product().Update(ctx, product)
}

func (h *productService) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	return h.storage.Product().Delete(ctx, key)
}
