package grpc

import (
	pb "catalog_service/genproto/catalog_service"
	"catalog_service/grpc/client"
	"catalog_service/pkg/logger"
	"catalog_service/service"
	"catalog_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *grpc.Server {
	grpcServer := grpc.NewServer()

	pb.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(strg, services, log))
	pb.RegisterProductServiceServer(grpcServer, service.NewProductService(strg, services, log))

	reflection.Register(grpcServer)

	return grpcServer
}
