package client

import (
	"catalog_service/config"
	"catalog_service/genproto/catalog_service"
	"google.golang.org/grpc"
)

type IServiceManager interface {
	CategoryService() catalog_service.CategoryServiceClient
	ProductService() catalog_service.ProductServiceClient
}

type grpcClients struct {
	categoryService catalog_service.CategoryServiceClient
	productService  catalog_service.ProductServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connUserService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		categoryService: catalog_service.NewCategoryServiceClient(connUserService),
		productService:  catalog_service.NewProductServiceClient(connUserService),
	}, nil
}

func (g *grpcClients) CategoryService() catalog_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() catalog_service.ProductServiceClient {
	return g.productService
}
