CREATE TYPE "product_type" AS ENUM ('modifier', 'product');

CREATE TABLE if not exists "categories" (
    "id" uuid PRIMARY KEY,
    "title" varchar,
    "status" BOOL,
    "parent_id" UUID,
    "order_number" SERIAL,
    "searching_column" TEXT,
    "created_at" timestamp DEFAULT (now()),
    "updated_at" timestamp DEFAULT (now()),
    "deleted_at" int DEFAULT 0
);

CREATE TABLE if not exists "products" (
    "id" uuid PRIMARY KEY,
    "description" TEXT,
    "order_number" SERIAL,
    "status" BOOL,
    "type" product_type,
    "price" BIGINT,
    "category_id" UUID REFERENCES categories(id),
    "searching_column" TEXT,
    "created_at" timestamp DEFAULT (now()),
    "updated_at" timestamp DEFAULT (now()),
    "deleted_at" int DEFAULT 0
);
